from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoTask


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list": lists,
    }
    return render(request, "lists/list.html", context)


def todo_list_detail(request, id):
    items = get_object_or_404(TodoList, id=id)
    context = {"todo_item": items}
    return render(request, "lists/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "lists/create.html", context)


def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = TodoForm(instance=update)

    context = {
        "form": form,
    }
    return render(request, "lists/edit.html", context)


def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")

    return render(request, "lists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoTask(request.POST)
        if form.is_valid():
            form.save()
            new_list = form.cleaned_data["list"]
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoTask()

    context = {
        "form": form,
    }
    return render(request, "lists/create_item.html", context)


def todo_item_update(request, id):
    item_update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoTask(request.POST, instance=item_update)
        if form.is_valid():
            item_update = form.save()
            new_list = form.cleaned_data["list"]
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoTask(instance=item_update)
    context = {"form": form}
    return render(request, "lists/edit_item.html", context)
